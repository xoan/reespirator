#ifndef PINOUT_H
#define PINOUT_H

// Stepper driver
#define PIN_STEPPER_STEP 6
#define PIN_STEPPER_DIRECTION 7
#define PIN_STEPPER_EN 8
#define PIN_STEPPER_ALARM 3

// Buzzer
#define PIN_BUZZ 11

// Stepper homing
#define PIN_STEPPER_ENDSTOP 2

// Solenoid
#define PIN_SOLENOID 39

// Relay
#define PIN_RELAY 25

// BME280 SPI for Arduino Nano or Mega 128
// #define PIN_BME_SCK  13
// #define PIN_BME_MISO 12
// #define PIN_BME_MOSI 11
// #define PIN_BME_CS1  10 // sensor de presion 1
// #define PIN_BME_CS2  4  // sensor de presion 2

// BME280 SPI for Arduino Mega 256
// #define PIN_BME_SCK  52
// #define PIN_BME_MISO 50
// #define PIN_BME_MOSI 51
// #define PIN_BME_CS1  53 // sensor de presion 1
// #define PIN_BME_CS2  49 // sensor de presion 2

#endif // ENCODER_H
